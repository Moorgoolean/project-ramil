package az.unnamedorg.projectx.model.DTO.response;

import lombok.Data;

@Data
public class AuthResponseDTO  {
    String tokenValue;
}
