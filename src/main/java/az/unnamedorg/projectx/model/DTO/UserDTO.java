package az.unnamedorg.projectx.model.DTO;

import lombok.Data;

@Data
public class UserDTO {
    String username;
    String password;
}
