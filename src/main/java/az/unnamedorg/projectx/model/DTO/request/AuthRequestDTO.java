package az.unnamedorg.projectx.model.DTO.request;

import lombok.Data;

@Data
public class AuthRequestDTO {
    String userName;
    String password;
}
