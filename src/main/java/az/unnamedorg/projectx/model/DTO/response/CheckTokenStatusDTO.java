package az.unnamedorg.projectx.model.DTO.response;

import az.unnamedorg.projectx.model.entity.ProjxUser;
import lombok.Data;

@Data
public class CheckTokenStatusDTO {
    boolean tokenActive;
    ProjxUser user = null;
}
