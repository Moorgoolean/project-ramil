package az.unnamedorg.projectx.model.DTO.request;

import lombok.Data;

@Data
public class RootDTO {
    String token;
}
