package az.unnamedorg.projectx.model.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="projx_user")
@Data
@RequiredArgsConstructor
public class ProjxUser extends Root {
    String phoneNumber;
    String username;
    String email;
    String salt;
    String hash;
}
