package az.unnamedorg.projectx.model.entity;

import az.unnamedorg.projectx.tools.StringTool;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Data
public class Root {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    private String createdDate = StringTool.getCurrentDate();
    private String updatedDate = StringTool.getCurrentDate();

}
