package az.unnamedorg.projectx.model.entity;


import az.unnamedorg.projectx.tools.StringTool;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

import static az.unnamedorg.projectx.tools.ConfigurationStrings.TOKEN_TIMEOUT;


@Entity
@Table(name = "projx_token")
@Data
@NoArgsConstructor
public class ProjxToken extends Root{
    @ManyToOne(targetEntity = ProjxUser.class,fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_user_id")
    private ProjxUser userId;
    private String tokenValue = UUID.randomUUID().toString();
    private Long lastUpdated = System.currentTimeMillis();


    public ProjxToken(ProjxUser userId) {
        this.userId = userId;
    }

    public void refresh() {
        this.setUpdatedDate(StringTool.getCurrentDate());
        this.setLastUpdated(System.currentTimeMillis());
    }

    public boolean isExpired() {
        return System.currentTimeMillis() - lastUpdated > TOKEN_TIMEOUT;
    }
}
