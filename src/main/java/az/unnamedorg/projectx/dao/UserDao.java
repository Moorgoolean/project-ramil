package az.unnamedorg.projectx.dao;

import az.unnamedorg.projectx.model.entity.ProjxUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<ProjxUser, Long> {
    ProjxUser findFirstByUsername(String username);

    ProjxUser findFirstByEmail(String email);

    ProjxUser findFirstByPhoneNumber(String phoneNumber);

    boolean existsByUsername(String username);

    boolean existsByEmail(String email);

}
