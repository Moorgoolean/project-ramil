package az.unnamedorg.projectx.dao;


import az.unnamedorg.projectx.model.entity.ProjxToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TokenDao extends JpaRepository<ProjxToken, Long> {
    boolean existsByUserId_Id(Long userId);

    ProjxToken findFirstByUserId_Id(Long userId);

    ProjxToken findFirstByUserId_PhoneNumber(String phoneNumber);

    ProjxToken findFirstByUserId_Username(String username);

    ProjxToken findFirstByTokenValue(String tokenValue);

}
