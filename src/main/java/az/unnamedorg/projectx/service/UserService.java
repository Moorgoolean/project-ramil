package az.unnamedorg.projectx.service;


import az.unnamedorg.projectx.model.DTO.UserDTO;
import az.unnamedorg.projectx.model.DTO.request.AuthRequestDTO;
import az.unnamedorg.projectx.model.DTO.response.AuthResponseDTO;
import az.unnamedorg.projectx.model.DTO.response.CheckTokenStatusDTO;
import az.unnamedorg.projectx.model.entity.ProjxUser;

public interface UserService {
    ProjxUser registerUser(UserDTO dto);
    AuthResponseDTO authorize(AuthRequestDTO dto);
    CheckTokenStatusDTO checkToken(String tokenVal);

}
