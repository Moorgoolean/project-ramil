package az.unnamedorg.projectx.service.impl;

import az.unnamedorg.projectx.dao.UserDao;
import az.unnamedorg.projectx.model.DTO.UserDTO;
import az.unnamedorg.projectx.model.DTO.request.AuthRequestDTO;
import az.unnamedorg.projectx.model.DTO.response.AuthResponseDTO;
import az.unnamedorg.projectx.model.DTO.response.CheckTokenStatusDTO;
import az.unnamedorg.projectx.model.entity.ProjxUser;
import az.unnamedorg.projectx.service.UserService;
import az.unnamedorg.projectx.tools.StringTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;


    @Override
    public ProjxUser registerUser(UserDTO dto) {
        ProjxUser user = new ProjxUser();
        String salt = StringTool.generateSalt(dto.getUsername() + dto.getPassword());
        user.setUsername(dto.getUsername());
        user.setHash(StringTool.getHashFromPass(dto.getPassword(), salt));
        return userDao.save(user);
    }

    @Override
    public AuthResponseDTO authorize(AuthRequestDTO dto) {
        return null;
    }

    @Override
    public CheckTokenStatusDTO checkToken(String tokenVal) {
        return null;
    }
}
