package az.unnamedorg.projectx.tools;

import java.util.regex.Pattern;

public class ConfigurationStrings {

    public static final long TOKEN_TIMEOUT = 2 * 24 * 60 * 60 * 1000;
    public static final long REGISTRATION_PENDING_TIMEOUT = 2 * 60 * 60 * 1000;

    public static final String PRE_GSALT = "SPIDERMAN";
    public static final String POST_GSALT = "DEBUGGER";

    public static final Pattern EMAIL_REGEX_PATTERN =  Pattern.compile("^([\\w- \\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}");

}
