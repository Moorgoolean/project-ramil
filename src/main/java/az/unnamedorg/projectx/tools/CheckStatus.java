package az.unnamedorg.projectx.tools;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CheckStatus {
    Boolean result;
    String message;
}
