package az.unnamedorg.projectx.tools;


import org.apache.tomcat.util.codec.binary.Base64;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import static az.unnamedorg.projectx.tools.ConfigurationStrings.*;


public class StringTool {

//    public static void main(String[] args) {
//        int i = 4;
//        System.out.println(++i);
//    }

    public static boolean checkDate(String date) {
        if (date == null) return false;
        return date.matches("[0-9]{4}-([0][1-9]|[1][0-2])-([0-2][1-9]|[3][0-1])");
    }

    public static String getCurrentDate() {
        return new SimpleDateFormat("yyyyMMdd").format(new Date());
    }

    public static byte[] decodeFromBase64(String string) {
        return  Base64.decodeBase64(string);
    }
    public static byte[] decodeFromBase64(byte[] bytes) {
        return Base64.decodeBase64(bytes);
    }
    public static String encodeToBase64(byte[] bytes) {
        return Base64.encodeBase64String(bytes);
    }


    public static String getUserFullName(String name, String surname, String middlename) {
        return (surname == null || surname.equals("") ? "" : surname) +
                " " + (name == null || name.equals("") ? "" : name) +
                " " + (middlename == null || middlename.equals("") ? "" : middlename);
    }

    public static String formatDate(String date, String inputFormat,String outputFormat){
        return formatDate(parseStrToDate(date,inputFormat),outputFormat);
    }
    public static String formatDate(Date date, String format){
        if(date==null) return "";
        DateFormat dfDate = new SimpleDateFormat(format);
        return dfDate.format(date);
    }


    public static Date parseStrToDate(String date,String format){
        if (date==null||date.equals("")) return null;
        DateFormat dfDate = new SimpleDateFormat(format);
        try {
            return dfDate.parse(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }


    public static String hashSha3_256(String text) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA3-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new RuntimeException("No such algorithm");
        }
        final byte[] hashBytes = digest.digest(text.getBytes());
        StringBuilder stringValue = new StringBuilder();
        for (byte b : hashBytes) {
            stringValue.append(String.format("%02x", b));
        }
        return stringValue.toString();
    }


    public static Boolean compareToHash(String passwordSalted, String hash) {
        return hash.equals(hashSha3_256(PRE_GSALT + passwordSalted + POST_GSALT));
    }


    public static String getHashFromPass(String password, String salt) {
        return hashSha3_256(PRE_GSALT + password + salt + POST_GSALT);
    }

    public static String generate6Pin() {
        var pattern = "ABCDEFGHIJKLMNOPQRSTUWXYZ123456789";
        var code = new StringBuilder();
        new Random().ints(6, 0, pattern.length())
                .sequential().forEach(it -> {
            code.append(pattern.charAt(it));
        });
        return code.toString();
    }

    public static String generateSalt(String text) {
        return new String(java.util.Base64.getEncoder().encode(text.substring(0, 3).getBytes()));
    }

    public static CheckStatus checkUsername(String username) {
        if (username == null)
            return new CheckStatus(false,
                    "Username is null");
        if (username.length() <= 5)
            return new CheckStatus(false,
                    "Username is too short. Should be more than 4 symbols");
        if (!username.matches("[a-zA-Z][a-zA-Z0-9]{3,29}"))
            return new CheckStatus(false,
                    "Username shouldn't begin with a number and should only contain latin symbols");
        return new CheckStatus(true, "Username OK");
    }

    public static CheckStatus checkPassword(String password) {
        if (password == null) return new CheckStatus(false, "Password is null");
        if (password.length() <= 7)
            return new CheckStatus(false, "Password is too short. Should be more than 7 symbols and less than 30");
        if (!password.matches("[a-zA-Z0-9][a-zA-Z0-9_]{6,29}"))
            return new CheckStatus(false,
                    "Password should only contain latin symbols, numbers and under");
        return new CheckStatus(true, "Password OK");
    }

    public static CheckStatus checkEmail(String email) {
        if (email == null) return new CheckStatus(false, "Email is null");
        if (!checkEmailValid(email))
            return new CheckStatus(false,
                    "Wrong email format");
        return new CheckStatus(true, "Email OK");
    }

    public static CheckStatus checkPhoneNumber(String phoneNumber) {
        if (!phoneNumber.matches("\\+994((51)|(55)|(70)|(50))[0-9]{7}"))
            return new CheckStatus(false, "Bad phone number format");
        return new CheckStatus(true, "Number is ok");
    }

    public static CheckStatus checkUserId(String userId) {
        if (!userId.matches("[0-9a-zA-Z]{6}"))
            return new CheckStatus(false, "Wrong user id format");
        return new CheckStatus(true, "User id is ok");
    }

    public static boolean checkEmailValid(String email) {
        return email.matches(EMAIL_REGEX_PATTERN.toString());
    }


}
