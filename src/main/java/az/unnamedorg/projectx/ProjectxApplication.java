package az.unnamedorg.projectx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.test.context.ActiveProfiles;

@SpringBootApplication
@ActiveProfiles({"dev", "test"})
public class ProjectxApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectxApplication.class, args);
    }

}
